Turn your computer into a jumpscare simulator with this one simple Python 3 script!
To make the script work, you have to set the variable at the top: The time between jumpscares (in seconds). As-is, it is between 3 and 5 minutes.

You probibly should also replace the video that plays. You can do this by dragging another .MP4 named "video" into the project and deleting the one that is already there.